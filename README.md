# Mandiri Employee Management _Zikri Akmal S_

# ID

## Persyaratan

- Versi Node setidaknya ^18.13

## Cara Menjalankan Proyek

- Clone proyek ini menggunakan git atau cukup unduh proyek ini
- Jalankan `npm install` di terminal untuk menginstal dependensi
- Jalankan `npm run start` atau `ng serve` di terminal untuk memulai server pengembangan
- Buka di `http://localhost:4200/` browser
- Gunakan akun berikut untuk masuk

```json
{
  "email": "zikriakmale@gmail.com",
  "password": "rahasiakita"
}
```

---

# EN

## Project Summary

`This project is angular crud for employee management proudly created by zikri akmal s from scretch. This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 17.0.10.`

## Prerequisites

- Node Version at least ^18.13

## How To Run project

- Clone this project using git or just download this project
- Run `npm install` on terminal to install dependencies
- Run `npm run start` or `ng serve` on terminal to start development server
- Open `http://localhost:4200/` at browser
- Use this account for login

```json
{
  "email": "zikriakmale@gmail.com",
  "password": "rahasiakita"
}
```
