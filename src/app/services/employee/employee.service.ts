import { Injectable } from '@angular/core';
import {
  AuthSession,
  SupabaseClient,
  createClient,
} from '@supabase/supabase-js';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  private supabase: SupabaseClient;
  _session: AuthSession | null = null;

  constructor() {
    this.supabase = createClient(
      environment.supabaseUrl,
      environment.supabaseKey
    );
  }

  getAllEmployee() {
    return this.supabase
      .from('employee')
      .select()
      .order('id', { ascending: false });
  }

  addEmployee(employee: any) {
    const payload = {
      ...employee,
    };

    return this.supabase.from('employee').insert(payload).select();
  }

  updateEmployee(employee: any) {
    const payload = {
      ...employee,
    };

    return this.supabase
      .from('employee')
      .update(payload)
      .eq('id', employee.id)
      .select();
  }

  deleteEmployee(id: number) {
    return this.supabase.from('employee').delete().eq('id', id).select();
  }
}
