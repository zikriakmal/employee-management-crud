import { Injectable } from '@angular/core';
import {
  AuthSession,
  SupabaseClient,
  createClient,
} from '@supabase/supabase-js';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private supabase: SupabaseClient;
  _session: AuthSession | null = null;

  // private apiUrl: string = environment.supabaseUrl;

  constructor() {
    this.supabase = createClient(
      environment.supabaseUrl,
      environment.supabaseKey
    );
  }

  signIn(email: string, password: string) {
    return this.supabase.auth.signInWithPassword({
      email,
      password,
    });
  }

  logout(): void {
    localStorage.removeItem('user');
  }

  isAuthenticated(): any {
    const res = localStorage.getItem('user');
    return res;
  }
}
