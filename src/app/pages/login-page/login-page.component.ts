import { HttpClientModule } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { Router } from '@angular/router';
import { AuthReqInterface } from '../../interfaces/AuthInterface';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-login-page',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatIconModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    MatIconModule,
  ],
  templateUrl: './login-page.component.html',
  styleUrl: './login-page.component.css',
})
export class LoginPageComponent implements OnInit {
  isFormValid: boolean;
  isLoading: boolean;
  isWrongPassword: boolean;
  hide: boolean;

  constructor(private authService: AuthService, private router: Router) {
    this.isFormValid = false;
    this.isLoading = false;
    this.isWrongPassword = false;
    this.hide = true;
  }

  dataForm: AuthReqInterface | undefined;

  ngOnInit(): void {
    if (this.authService.isAuthenticated()) {
      this.router.navigate(['/dashboard']);
    }
  }

  onKeyupchangeInput(data: any): void {
    this.dataForm = { ...this.dataForm, [data.target.name]: data.target.value };
    if (
      this.dataForm?.email === '' ||
      this.dataForm?.email === undefined ||
      this.dataForm?.password === '' ||
      this.dataForm?.password === undefined
    ) {
      this.isFormValid = false;
    } else {
      this.isFormValid = true;
    }
  }

  toggleVisibilityPassword() {
    this.hide = !this.hide;
  }

  async onSubmit(): Promise<void> {
    this.isLoading = true;
    const email = this.dataForm?.email as string;
    const password = this.dataForm?.password as string;
    try {
      const { error, data } = await this.authService.signIn(email, password);
      if (error) {
        this.isLoading = false;
        this.isWrongPassword = true;
        return;
      }

      localStorage.setItem('user', JSON.stringify(data));
      this.isLoading = false;
      this.router.navigate(['/dashboard']);
    } catch {
      this.isLoading = false;
      alert('Terjadi Kesalahan Pada server');
    }
  }
}
