import { LiveAnnouncer } from '@angular/cdk/a11y';
import {
  AfterViewInit,
  Component,
  HostListener,
  Injector,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSort, MatSortModule, Sort } from '@angular/material/sort';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { Router } from '@angular/router';
import { Employee } from '../../interfaces/EmployeeInterface';
import { AuthService } from '../../services/auth/auth.service';
import { EmployeeService } from '../../services/employee/employee.service';
import { AddEmployeeComponent } from '../add-employee/add-employee.component';
import { DeleteEmployeeComponent } from '../delete-employee/delete-employee.component';
import { DetailEmployeeComponent } from '../detail-employee/detail-employee.component';
import { UpdateEmployeeComponent } from '../update-employee/update-employee.component';

@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [
    MatTableModule,
    MatSortModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatDialogModule,
  ],
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.css',
})
export class DashboardComponent implements OnInit, AfterViewInit {
  isMobile: boolean = false;
  employeeData?: Employee[];

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort = new MatSort();

  @HostListener('window:resize', ['$event'])
  onResize(event: Event): void {
    this.checkScreenSize();
  }

  dataSource = new MatTableDataSource<Employee>(this.employeeData);
  constructor(
    private _liveAnnouncer: LiveAnnouncer,
    private authService: AuthService,
    private router: Router,
    private dialog: MatDialog,
    private injector: Injector,
    private employeeService: EmployeeService
  ) {}

  showFiller = true;
  displayedColumns: string[] = [
    'username',
    'email',
    'firstName',
    'lastName',
    'action',
  ];

  openAddDialog() {
    const dialogRef = this.dialog.open(AddEmployeeComponent, {
      data: {},
      injector: this.injector,
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getAllEmployeeData();
    });
  }

  openDetailDialog(employee: Employee) {
    const dialogRef = this.dialog.open(DetailEmployeeComponent, {
      data: { employee: employee },
      injector: this.injector,
    });
  }

  openDeleteDialog(employee: Employee) {
    const dialogRef = this.dialog.open(DeleteEmployeeComponent, {
      data: {
        employee: employee,
      },
      injector: this.injector,
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getAllEmployeeData();
    });
  }

  openUpdateDialog(employee: Employee) {
    const dialogRef = this.dialog.open(UpdateEmployeeComponent, {
      data: { employee: employee },
      injector: this.injector,
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.getAllEmployeeData();
    });
  }

  async ngOnInit(): Promise<void> {
    this.checkScreenSize();
    if (!this.authService.isAuthenticated()) {
      this.router.navigate(['/']);
    }
    await this.getAllEmployeeData();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  announceSortChange(sortState: Sort) {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
      console.log(
        this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`)
      );
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

  async getAllEmployeeData(): Promise<void> {
    const { data } = await this.employeeService.getAllEmployee();
    this.employeeData = data as any;
    this.dataSource.data = this.employeeData as any;
  }

  logout(): void {
    this.authService.logout();
    this.router.navigate(['/']);
  }

  checkScreenSize(): void {
    this.isMobile = window.innerWidth < 768; // Adjust the breakpoint as needed
  }
}
