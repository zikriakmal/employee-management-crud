import { CurrencyPipe, DatePipe, JsonPipe } from '@angular/common';
import { Component, Inject } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { Employee } from '../../interfaces/EmployeeInterface';
import { EmployeeService } from '../../services/employee/employee.service';

@Component({
  selector: 'app-update-employee',
  standalone: true,
  imports: [
    DatePipe,
    CurrencyPipe,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatRadioModule,
    MatSelectModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    FormsModule,
    JsonPipe,
  ],
  templateUrl: './update-employee.component.html',
  styleUrl: './update-employee.component.css',
})
export class UpdateEmployeeComponent {
  employeeData: Employee;

  minDate: Date;
  dataForm: any;
  isFormValid: boolean;
  isLoading: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { employee: Employee },
    public dialogRef: MatDialogRef<UpdateEmployeeComponent>,
    private employeeService: EmployeeService
  ) {
    this.employeeData = data.employee as Employee;
    this.minDate = new Date();
    this.isFormValid = false;
    this.isLoading = false;
    this.dataForm = data.employee as Employee;
  }

  closeDialog() {
    this.dialogRef.close();
  }

  onKeyupchangeInput(data: any): void {
    if (data?.target?.name === undefined) {
      this.dataForm = { ...this.dataForm, [data.source.name]: data.value };
    } else {
      this.dataForm = {
        ...this.dataForm,
        [data.target.name]: data.target.value,
      };
    }
    this.checkFormValid();
  }

  onGroupChange(data: any): void {
    this.dataForm = { ...this.dataForm, group: data.value };
    this.checkFormValid();
  }

  onDateChange(data: any): void {
    const date = new Date(data.value).toISOString();
    this.dataForm = { ...this.dataForm, birthDate: date };
    this.checkFormValid();
  }

  checkFormValid(): void {
    if (
      this.dataForm?.username === '' ||
      this.dataForm?.username === undefined ||
      this.dataForm?.email === '' ||
      this.dataForm?.email === undefined ||
      this.dataForm?.firstName === '' ||
      this.dataForm?.firstName === undefined ||
      this.dataForm?.lastName === '' ||
      this.dataForm?.lastName === undefined ||
      this.dataForm?.description === '' ||
      this.dataForm?.description === undefined ||
      this.dataForm?.status === '' ||
      this.dataForm?.status === undefined ||
      this.dataForm?.basicSalary === 0 ||
      this.dataForm?.basicSalary === undefined ||
      this.dataForm?.group === undefined ||
      this.dataForm?.birthDate === undefined
    ) {
      this.isFormValid = false;
    } else {
      this.isFormValid = true;
    }
  }

  async onSubmit(): Promise<void> {
    this.isLoading = true;
    const { error, data } = await this.employeeService.updateEmployee(
      this.dataForm
    );
    console.log(error, data, 'tas');
    this.closeDialog();
    this.isLoading = false;
  }
}
