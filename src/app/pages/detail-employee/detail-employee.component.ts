import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Employee } from '../../interfaces/EmployeeInterface';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-detail-employee',
  standalone: true,
  imports: [DatePipe, CurrencyPipe, MatButtonModule],
  templateUrl: './detail-employee.component.html',
  styleUrl: './detail-employee.component.css',
})
export class DetailEmployeeComponent implements OnInit {
  datums: Employee;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { employee: Employee },
    public dialogRef: MatDialogRef<DetailEmployeeComponent>
  ) {
    this.datums = data.employee as Employee;
  }

  ngOnInit(): void {}

  closeDialog() {
    this.dialogRef.close();
  }
}
