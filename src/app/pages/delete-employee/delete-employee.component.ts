import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Employee } from '../../interfaces/EmployeeInterface';
import { MatButtonModule } from '@angular/material/button';
import { EmployeeService } from '../../services/employee/employee.service';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
@Component({
  selector: 'app-delete-employee',
  standalone: true,
  imports: [MatButtonModule, MatProgressSpinnerModule],
  templateUrl: './delete-employee.component.html',
  styleUrl: './delete-employee.component.css',
})
export class DeleteEmployeeComponent {
  dataEmployee: Employee;
  isLoading: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: { employee: Employee },
    public dialogRef: MatDialogRef<DeleteEmployeeComponent>,
    private employeeService: EmployeeService
  ) {
    this.dataEmployee = data.employee as Employee;
    this.isLoading = false;
  }

  async deleteAction(): Promise<void> {
    this.isLoading = true;
    const { error, data } = await this.employeeService.deleteEmployee(
      this.dataEmployee.id
    );
    this.dialogRef.close();
    this.isLoading = false;
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
